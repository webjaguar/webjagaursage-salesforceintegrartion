package com.webjaguarsage.integration.helpher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguarsage.integration.config.Constants;
import com.webjaguarsage.integration.entity.*;
import org.apache.commons.httpclient.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.*;


/**
 * Created by soujanya
 */
public class HttpHelperImlTest {

    private HttpHelper httpHelper;
    private AccountAuthorizationConfig authorizationConfig;
    private HttpClient client;/*
    private final String clientId ="3MVG98_Psg5cppyZ7D3jOCSXgVgkWHq81OIuw6RnGVFuWxgsCqmmybWShacP8Z_q12iG9X8NNjLwl_DhrAWo9";
    private final String secretKey = "857873956965841785";
    private final String username = "souji@sagelive.advancedemedia.dev";
    private final String passwrod = "12345AeM";*/

    private final String clientId ="3MVG967gVD5fuTmI8ifAQBb9EvKSvs.91O8BJoGojdxcycyHnOab5Nf4nTsElFiLtpF5NbiyGA3KkSzFV6Hcg";
    private final String secretKey = "21F20DC240B9903FBA38F788569C9972E71D318DFDAF85555F7D6EBEE26A47F1";
    private final String username = "rakesh@webjaguar.com";
    private final String passwrod = "Webjaguar2020";

    @Before
    public void setup(){
        client = new HttpClient();
        authorizationConfig = new AccountAuthorizationConfig();
        authorizationConfig.setClientId(clientId);
        authorizationConfig.setClientSecretKey(secretKey);
        authorizationConfig.setUserName(username);
        authorizationConfig.setPassword(passwrod);
        authorizationConfig.setGrantType("password");
        authorizationConfig.setSandBox(true);
        System.out.println("AUTHORIZATIONConfig call");
        httpHelper = new HttpHelperIml(client,authorizationConfig);
    }

    @Test
    public void testGetAuthorizationToken() throws Exception {
        httpHelper.getAuthorizationToken();
    }

//    @Test
//    public void testPostJournal() throws IOException, WebjaguarSageException, JSONException {
//        httpHelper.getAuthorizationToken();
//        httpHelper.postJournal(getTestProductJSONPost(),"product");
//    }
//
//    @Test
//    public void testInsertAccount() throws Exception, WebjaguarSageException, JSONException {
//       httpHelper.getAuthorizationToken();
//       httpHelper.postJsonString(getTestAccount(), "Account");
//    }
//
//    @Test
//    public void testInsertContact() throws Exception, WebjaguarSageException, JSONException {
//        httpHelper.getAuthorizationToken();
//        httpHelper.postJsonString(getTestContact(), "Contact");
//    }
//
//    @Test
//    public void testInsertOpportunity() throws Exception, WebjaguarSageException, JSONException  {
//        httpHelper.getAuthorizationToken();
//        httpHelper.postJsonString(createCompositeRequest(), "composite");
//    }

    public Account getTestAccount() {
        Account account = new Account();
        account.setName("ACCOUNT_2025 CODE");
        account.setBillingCity("Aliso Viejo");
        account.setBillingCountry("USA");
        account.setBillingState("California");
        account.setBillingPostalCode("92656");
        account.setBillingStreet("81 Columbia 2025");
        /*
        Map<String, String> customMap = new HashMap<String, String>();
        customMap.put("Sync_to_Fishbowl__c", "false");
        account.setJsonMap(customMap);*/
        //account.setShippingAddress(generateAddress());

        return account;
    }

    public Contact getTestContact() {
        Contact contact = new Contact();
        contact.setAccountId("0014100000JA1FYAA1");
        contact.setEmail("souji2@codeTest.com");
        contact.setFirstName("ACONTACT_2");
        contact.setLastName("CODE");
        contact.setMailingCity("Aliso Viejo");
        contact.setMailingCountry("USA");
        contact.setMailingState("California");
        contact.setMailingStreet("81 Columbia");
        return contact;
    }

    public  CompositeRequestRecord getAccountCompositeRequestRecord() {

        CompositeRequestRecord accountRecord = new CompositeRequestRecord();
        accountRecord.setMethod("GET");
        accountRecord.setUrl(Constants.API_SF_URL+Constants.SOBJECT+"Account/webCustomExternalId__c/WEB_ACCOUNT_6");
        accountRecord.setReferenceId("refAccount");

        return accountRecord;
    }

    public  CompositeRequestRecord getContactCreationCompositeRequestRecord() {

        CompositeRequestRecord contactCreationRecord = new CompositeRequestRecord();
        contactCreationRecord.setMethod("POST");
        contactCreationRecord.setUrl(Constants.API_SF_URL+Constants.SOBJECT + "Contact");
        contactCreationRecord.setReferenceId("refContactCreation");
        Contact contact = new Contact();
        contact.setAccountId("@{refAccount.Id}");
        contact.setEmail("souji2@codeTest.com");
        contact.setFirstName("ACONTACT_2");
        contact.setLastName("CODE");
        contact.setMailingCity("Aliso Viejo");
        contact.setMailingCountry("USA");
        contact.setMailingState("California");
        contact.setMailingStreet("81 Columbia");
        contactCreationRecord.setBody(contact);

        return contactCreationRecord;
    }

    public  CompositeRequestRecord getContactCompositeRequestRecord() {

        CompositeRequestRecord contactRecord = new CompositeRequestRecord();
        contactRecord.setMethod("GET");
        contactRecord.setUrl(Constants.API_SF_URL+Constants.SOBJECT+"Contact/webCustomExternalId__c/WEB_ACONTACT_2");
        contactRecord.setReferenceId("refContact");

        return contactRecord;
    }


    public PricebookEntry getPricebookEntry() {
        PricebookEntry priceBookEntry = new PricebookEntry();
        priceBookEntry.setIsActive(true);
        priceBookEntry.setPricebook2Id("");
        return null;
    }

    public ProductJSONPost getTestProductJSONPost() {
        ProductJSONPost product = new ProductJSONPost();
        JournalObject ref = new JournalObject();
        ref.setName("ProductFROMWJSERVICE");
        ref.setUid("ProductFROMWJSERVICE");
        product.setReference(ref);
        product.setMessages(new ArrayList<String>());
        product.setFieldValueMap(new HashMap<String, String>());
        product.setFieldsValueMap(new HashMap<String, String>());
        List<Tag> tagList = new ArrayList<Tag>();
        Tag tag = new Tag();
        tag.setAccount(new JournalObject());
        tag.setCompany(new JournalObject());
        tag.setActive(true);
        ref = new JournalObject();
        ref.setName("Product");
        tag.setDimension(ref);
        tag.setForeignCredit(0.0);
        tag.setForeignDebit(0.0);
        tag.setIsPublic(true);
        tag.setLedgerAccount(new JournalObject());
        tag.setProduct(new JournalObject());
        ref = new JournalObject();
        ref.setName("Product Tag FROM WJ");
        ref.setUid("ProductTagFROMWJ");
        tag.setReference(ref);
        tag.setTagCurrency(new JournalObject());
        tag.setUnit(new JournalObject());
        tag.setUnitCredit(0.0);
        tag.setUnitDebit(0.0);
        tag.setUnitValue(50.0);
        tag.setBaseCredit(0.0);
        tag.setBaseCreditLimit(0.0);
        tag.setBaseDebit(0.0);
        tag.setMessages(new ArrayList<String>());
        tag.setFieldsValueMap(new HashMap<String, String>());
        tag.setFieldValueMap(new HashMap<String, String>());
        tagList.add(tag);
        product.setTags(tagList);
        return product;
    }
}