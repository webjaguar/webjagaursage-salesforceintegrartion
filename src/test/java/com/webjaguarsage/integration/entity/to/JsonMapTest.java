package com.webjaguarsage.integration.entity.to;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by soujanya
 */
public class JsonMapTest {

    private ObjectMapper mapper = new ObjectMapper();

    private String testData = "{\n" +
            "  \"Name\": \"SFOpp_10003\",\n" +
            "  \"Description\": \"Tesing the opportunity option\",\n" +
            "  \"StageName\": \"Closed Won\",\n" +
            "  \"Amount\": 50,\n" +
            "  \"TotalOpportunityQuantity\": 10,\n" +
            "  \"CloseDate\": \"2017-02-22\",\n" +
            "  \"Type\": \"Existing Customer - Upgrade\",\n" +
            "  \"OrderNumber__c\": \"0001\",\n" +
            "  \"AccountId\":\"0014100000JA1FYAA1\"\n" +
            "}";
    @Test
    public void testJsonSerilization() throws JsonProcessingException {
        JsonMap map = new JsonMap();
        map.put("name","Name");
        map.put("descryption","");
        map.put("integer",23);
        Map a = new HashMap();
        a.put("name","Name");
        a.put("descryption","");
        a.put("integer",23);
        map.put("map",a);
        System.out.println(mapper.writeValueAsString(map));
    }

}