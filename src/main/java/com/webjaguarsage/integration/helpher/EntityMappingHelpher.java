package com.webjaguarsage.integration.helpher;

import com.webjaguarsage.integration.entity.RequiredFieldAbstract;

import java.util.Map;

/**
 * Created by soujanya
 */
public class EntityMappingHelpher {

    public Map<String,Object> mergeRequiredFiledAndMap(RequiredFieldAbstract a, Map result) {
        result.putAll(a.getRequiredFiledMap());
        return result;
    }


}
