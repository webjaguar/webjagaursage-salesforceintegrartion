package com.webjaguarsage.integration.helpher;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguarsage.integration.config.Constants;
import com.webjaguarsage.integration.entity.*;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.apache.commons.httpclient.HttpClient;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by soujanya
 */
public class HttpHelperIml implements HttpHelper {

    private HttpClient client;
    private final ObjectMapper mapper = new ObjectMapper();
    private final AccountAuthorizationConfig authorizationConfig;
    private String accessKey;
    private String instanceUrl;

    public HttpHelperIml(HttpClient client,AccountAuthorizationConfig authorizationConfig) {
        this.client = client;
        this.authorizationConfig = authorizationConfig;
    }

    public void getAuthorizationToken() throws WebjaguarSageException {
    	PostMethod post = new PostMethod(authorizationConfig.isSandBox() ? Constants.SANDBOX_AUTHORIZATION_UL : Constants.AUTHORIZATION_UL);
        post.addParameter("grant_type", authorizationConfig.getGrantType());
        post.addParameter("client_id", authorizationConfig.getClientId());
        post.addParameter("client_secret", authorizationConfig.getClientSecretKey());
        post.addParameter("username", authorizationConfig.getUserName());
        post.addParameter("password", authorizationConfig.getPassword());
        
        try {
            new org.apache.commons.httpclient.HttpClient().executeMethod(post);
            JSONObject authResponse = new JSONObject(new JSONTokener(new InputStreamReader(post.getResponseBodyAsStream())));
            accessKey = authResponse.getString("access_token");
            instanceUrl = authResponse.getString("instance_url");
            System.out.println("Successfully Authenticated");
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebjaguarSageException(Constants.AUTHORIZATION_ERROR_MESSAGE);
        }
        if(accessKey == null || instanceUrl == null) {
            throw new WebjaguarSageException(Constants.AUTHORIZATION_ERROR_MESSAGE);
        }
    }

    public String checkIfExists(Object object, String type) throws IOException, JSONException {
        String query = null;

        if(type == "Account") {
            Account account = (Account) object;
            query = "SELECT id From " + type + " WHERE Name =" + "'" + account.getName() + "'";
        } else if(type == "Contact")  {
            Contact contact = (Contact) object;
            String name = contact.getFirstName() + " " + contact.getLastName();
            query = "SELECT id From " + type + " WHERE Name =" + "'" + name + "' AND Email =" +"'"+contact.getEmail()+"'" ;
        }

        GetMethod get = new GetMethod(instanceUrl+Constants.API_SF_URL+Constants.QUERY);
        get.setQueryString(new NameValuePair[] { new NameValuePair("q", query)});


        get.setRequestHeader("Content-type", "application/json");
        get.setRequestHeader("Authorization", "Bearer "+accessKey);

        BufferedReader br = null;
        String jsonResponse = null;
        try{
            System.out.println("URI: " + get.getURI());
            int returnCode = client.executeMethod(get);
            System.out.println("returnCode: " + returnCode);
            if(returnCode == HttpStatus.SC_BAD_REQUEST) {
                System.err.println("BAD REQUEST: " +get.getURI());
                get.getResponseBodyAsString();

            } else if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
                System.err.println("The Post method is not implemented by this URI");
                // still consume the response body
                get.getResponseBodyAsString();
            } else {

                br = new BufferedReader(new InputStreamReader(get.getResponseBodyAsStream()));
                String readLine;
                while(((readLine = br.readLine()) != null)) {
                    //System.err.println("Response: "+ readLine);
                    jsonResponse = readLine;
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        } finally {
            get.releaseConnection();
        }

        return jsonResponse;
    }
    
    public CreateOrUpdateResponse createOrUpdate(SFObject object, String type) {
    	CreateOrUpdateResponse result = new CreateOrUpdateResponse();
        result.setId(object.getId());
        result.setSuccess(false);
        
    	PostMethod post;
    	boolean isUpdate = (object.getId()!=null && !object.getId().isEmpty());
    	if(isUpdate) {
    		String url = instanceUrl+Constants.API_SF_URL+Constants.SOBJECT +type+"/"+object.getId();
    		post = new PostMethod(url) {
    			@Override public String getName() { return "PATCH"; }
    		};
    		object.setId(null);
    	} else {
    		String url = instanceUrl+Constants.API_SF_URL+Constants.SOBJECT +type;
    		post = new PostMethod(url);	
    	}
    	
    	post.setRequestHeader("Content-type", "application/json");
        post.setRequestHeader("Authorization", "Bearer "+accessKey);

        try {
        	String jsonString = this.mapper.writeValueAsString(object);
        	post.setRequestEntity(new StringRequestEntity(jsonString));
        	int clientCode = client.executeMethod(post);
        	
        	if(isUpdate){
        		//update case
        		if(post.getResponseBodyAsStream()==null) {
        			result.setSuccess(true);	
        		}
        		else {
        			BufferedReader br = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));
        			result.setSuccess(false);
        			result.setJsonResponse(br.readLine());
        		}
        		post.releaseConnection();
        		return result;
        	} else {
        		//create case
        		BufferedReader br = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));
        		String jsonResponse = br.readLine();
    	        ObjectMapper objectMapper = new ObjectMapper();
    			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    			if(clientCode == 400) {
    				//array of message errors
    			}
    			else {
    				result = objectMapper.readValue(jsonResponse, CreateOrUpdateResponse.class);
    			}
                result.setJsonResponse(jsonResponse);
                post.releaseConnection();
                return result;
            }
        } catch (Exception e) {
            e.printStackTrace();  
        }
        post.releaseConnection();
        return result;
    }
    
    public DeleteResponse delete(String id, String type) {
    	DeleteResponse result = new DeleteResponse();
        result.setSuccess(false);
        
    	String url = instanceUrl+Constants.API_SF_URL+Constants.SOBJECT +type+"/"+id;
    	PostMethod post = new PostMethod(url) {
			@Override public String getName() { return "DELETE"; }
		};
    	
    	post.setRequestHeader("Content-type", "application/json");
        post.setRequestHeader("Authorization", "Bearer "+accessKey);

        try {
        	int clientCode = client.executeMethod(post);
        	if(clientCode == 204) {
				//delete successfully
        		result.setSuccess(true);
			}
			else {
				BufferedReader br = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));
	        	result.setJsonResponse(br.readLine());
			}
        } catch (Exception e) {
            e.printStackTrace();  
        }
        post.releaseConnection();
        return result;
    }
    
    public String query(String queryStr) {
    	ObjectMapper jsonMapper = new ObjectMapper();
        List<Object> records = new ArrayList<Object>();
        try{
        	GetMethod get = new GetMethod(instanceUrl+Constants.API_SF_URL+Constants.QUERY);
            get.setQueryString(new NameValuePair[] { new NameValuePair("q", queryStr)});
            get.setRequestHeader("Content-type", "application/json");
            get.setRequestHeader("Authorization", "Bearer "+accessKey);
            client.executeMethod(get);
        	BufferedReader br = new BufferedReader(new InputStreamReader(get.getResponseBodyAsStream()));
            HashMap<String, Object> result = jsonMapper.readValue(br.readLine(), HashMap.class);
            get.releaseConnection();
            records.addAll((List<Object>)result.get("records"));
            
            if(!(Boolean)result.get("done")) {
            	while(true) {
                	String nextRecordsUrl = (String)result.get("nextRecordsUrl");
                	get = new GetMethod(instanceUrl+nextRecordsUrl);
                	get.setRequestHeader("Content-type", "application/json");
                    get.setRequestHeader("Authorization", "Bearer "+accessKey);
                    client.executeMethod(get);
                    br = new BufferedReader(new InputStreamReader(get.getResponseBodyAsStream()));
                    result = jsonMapper.readValue(br.readLine(), HashMap.class);
                    get.releaseConnection();
                    records.addAll((List<Object>)result.get("records"));
                    if((Boolean)result.get("done"))
                    	break;
            	}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        HashMap<String,Object> result = new HashMap<String,Object>();
        result.put("totalSize", records.size());
        result.put("records", records);
        
        try {
        	return jsonMapper.writeValueAsString(result);
        }
        catch(Exception e) {}
        
        return null;
    }
    
    public String postJournal(Object object,String type) throws IOException, JSONException {
        PostMethod post = new PostMethod(instanceUrl+Constants.API_BASE_URL+Constants.VERSION+type);

        post.setRequestHeader("Content-type", "application/json");
        post.setRequestHeader("Authorization", "Bearer "+accessKey);
        String jsonString = this.mapper.writeValueAsString(object);
        System.out.println(jsonString);
        if(type == "product") {
            post.setRequestEntity(new StringRequestEntity("[" + jsonString + "]"));
        } else {
            post.setRequestEntity(new StringRequestEntity(jsonString));
        }

        BufferedReader br = null;
        String jsonResponse = null;
        try{
            int returnCode = client.executeMethod(post);

            if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
                System.err.println("The Post method is not implemented by this URI");
                // still consume the response body
                post.getResponseBodyAsString();
            } else {
                br = new BufferedReader(new InputStreamReader(post.getResponseBodyAsStream()));
                String readLine;
                while(((readLine = br.readLine()) != null)) {
                    jsonResponse = readLine;
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        } finally {
            post.releaseConnection();
            if(br != null) try { br.close(); } catch (Exception fe) {}
        }
        System.out.println("jsonResponse:" + jsonResponse);
        return jsonResponse;
    }
}
