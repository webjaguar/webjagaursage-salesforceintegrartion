package com.webjaguarsage.integration.helpher;

import java.io.IOException;

import org.json.JSONException;

import com.webjaguarsage.integration.entity.CreateOrUpdateResponse;
import com.webjaguarsage.integration.entity.DeleteResponse;
import com.webjaguarsage.integration.entity.SFObject;
import com.webjaguarsage.integration.entity.WebjaguarSageException;

/**
 * Created by soujanya
 */
public interface HttpHelper {
    void getAuthorizationToken() throws WebjaguarSageException;
    DeleteResponse delete(String id,String type);
    CreateOrUpdateResponse createOrUpdate(SFObject object,String type);
    String query(String queryStr);
    String postJournal(Object object,String type) throws IOException, JSONException;
}
