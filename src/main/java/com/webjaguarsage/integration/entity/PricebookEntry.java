package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;

import javax.xml.ws.ServiceMode;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class PricebookEntry {

    private String product2Id;
    private String pricebook2Id;
    private String unitPrice;
    private Boolean isActive;
    private Boolean useStandardPrice;
    private String webCustomExternalId__c;
}
