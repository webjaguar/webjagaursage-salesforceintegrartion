package com.webjaguarsage.integration.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class Product2 extends SFObject {
	
	@JsonProperty("ProductCode")
    private String productCode;
    
    @JsonProperty("Description")
    private String description;
    
    @JsonProperty("Name")
    private String name;
    
    @JsonProperty("IsActive")
    private Boolean active;
}
