package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class CompositeRequest {

    private boolean allOrNone;
    private List<CompositeRequestRecord> compositeRequest;
}
