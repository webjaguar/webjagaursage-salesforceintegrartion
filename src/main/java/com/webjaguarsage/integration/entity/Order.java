package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class Order extends SFObject {
	@JsonProperty("Id")
	private String id;
	
	@JsonProperty("AccountId")
	private String accountId;
    
    @JsonProperty("EffectiveDate")
    private String effectiveDate;
    
    @JsonProperty("Status")
    private String status;
    
}