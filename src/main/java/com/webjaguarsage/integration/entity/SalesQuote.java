package com.webjaguarsage.integration.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
/**
 * Created by soujanya
 */
@Getter
@Setter
public class SalesQuote extends SFObject {
	@JsonProperty("Name")
	String name;
	
}