package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by soujanya
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class SFObject {
	@JsonProperty("attributes")
    private Attributes attributes;
	
	@JsonProperty("Id")
    private String id;
	
	@JsonIgnore
	private Map<String, String> customFields = new HashMap<String, String>();
    
    //Capture all other fields that Jackson do not match other members
    @JsonAnySetter
    void setCustomFields(String key, String value) {
    	if(key.endsWith("__c")) {
    		customFields.put(key, value);
		}
    }
    
    @JsonAnyGetter
    public Map<String, String> getCustomFields(){
    	return customFields;
    }
    
    public Object getField(String fieldName) {
    	if(fieldName.endsWith("__c")) {
    		return getCustomFields().get(fieldName);
    	}
    	try {
	    	Method method = this.getClass().getMethod("get"+fieldName);
			return method.invoke(this);
    	}
    	catch(Exception e) {}
    	return null;
    }
}