package com.webjaguarsage.integration.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class OpportunityLineItem extends SFObject {
	
	@JsonProperty("OpportunityId")
    private String opportunityId;
	
	@JsonProperty("Product2Id")
    private String productId;
	
	@JsonProperty("ProductCode")
    private String productCode;
	
	@JsonProperty("Quantity")
    private Integer quantity;
	
	@JsonProperty("TotalPrice")
    private Double totalPrice;
	
	@JsonProperty("UnitPrice")
    private Double unitPrice;
	
	@JsonProperty("PricebookEntryId")
    private String pricebookEntryId;
	
	@JsonProperty("Description")
    private String description;
}
