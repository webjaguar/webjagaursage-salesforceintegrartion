package com.webjaguarsage.integration.entity;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product2List extends SFObject {
	
	private int totalSize;

	@JsonProperty("records")
    private Product2[] product2s;
}
