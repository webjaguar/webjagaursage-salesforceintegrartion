package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class NewAccount implements RequiredFieldAbstract {
    private String name;
    private String accountNumber;
    private String billingStreet;
    private String billingCity;
    private String billingState;
    private String billingPostalCode;
    private String billingCountry;
    private String billingLatitude;
    private String billingLongitude;
    private String billingGeocodeAccuracy;
    private String shippingStreet;
    private String shippingCity;
    private String shippingState;
    private String shippingPostalCode;
    private String shippingCountry;
    private String shippingLatitude;
    private String shippingLongitude;
    private String shippingGeocodeAccuracy;
    private String phone;
    private String fax;
    private String website;

    public Map getRequiredFiledMap() {
        Map map = new HashMap();
        map.put("name",name);
        map.put("accountNumber",accountNumber);
        map.put("billingStreet",billingStreet);
        map.put("billingCity",billingCity);
        map.put("billingState",billingState);
        map.put("billingPostalCode",billingPostalCode);
        map.put("billingCountry",billingCountry);
        map.put("billingLongitude",billingLongitude);
        map.put("billingGeocodeAccuracy",billingGeocodeAccuracy);
        map.put("shippingStreet",shippingStreet);
        map.put("shippingCity",shippingCity);
        map.put("shippingState",shippingState);
        map.put("shippingPostalCode",shippingPostalCode);
        map.put("shippingCountry",shippingCountry);
        map.put("shippingLatitude",shippingLatitude);
        map.put("shippingLongitude",shippingLongitude);
        map.put("shippingGeocodeAccuracy",shippingGeocodeAccuracy);
        map.put("phone",phone);
        map.put("fax",fax);
        map.put("website",website);
        return map;
    }
}