package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

@Getter
@Setter
public class OpportunityList extends SFObject {
	
	private Integer totalSize;

	@JsonProperty("records")
    private Opportunity[] opportunities;
}
