package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class Opportunity extends SFObject {
	@JsonProperty("Id")
	private String id;
	
	@JsonProperty("AccountId")
    private String accountId;
	
	@JsonProperty("ContactId")
    private String contactId;
    
	@JsonProperty("Amount")
    private Double amount;
    
	@JsonProperty("CloseDate")
    private String closeDate;
    
	@JsonProperty("Description")
    private String description;
    
	@JsonProperty("Name")
    private String name;
	
	@JsonProperty("StageName")
    private String stageName;
}
