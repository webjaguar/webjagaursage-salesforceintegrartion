package com.webjaguarsage.integration.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpportunityLineItemList extends SFObject {
	
	private String totalSize;

	@JsonProperty("records")
    private OpportunityLineItem[] lineItems;
}

