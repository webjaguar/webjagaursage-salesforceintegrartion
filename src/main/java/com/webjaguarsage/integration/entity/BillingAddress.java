package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class BillingAddress {

    private String billingCity;
    private String billingCountry;
    private String billingCountryCode;
    private String billingGeocodeAccuracy;
    private String billingPostalCode;
    private String billingState;
    private String billingStateCode;
    private String billingStreet;
}
