package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class Contact extends SFObject {
	
	@JsonProperty("CreatedDate")
	private String createdDate;
	
    @JsonProperty("Name")
    private String Name;
    
    @JsonProperty("Email")
    private String email;
    
    @JsonProperty("AccountId")
	private String accountId;
    
    @JsonProperty("HomePhone")
    private String homePhone;
    
    @JsonProperty("FirstName")
    private String firstName;
    
    @JsonProperty("LastName")
    private String lastName;
    
    @JsonProperty("MailingStreet")
    private String mailingStreet;
    
    @JsonProperty("MailingCity")
    private String mailingCity;
    
    @JsonProperty("MailingState")
    private String mailingState;
    
    @JsonProperty("MailingStateCode")
    private String mailingStateCode;
    
    @JsonProperty("MailingPostalCode")
    private String mailingPostalCode;
    
    @JsonProperty("MailingCountry")
    private String mailingCountry;
    
    @JsonProperty("MailingCountryCode")
    private String mailingCountryCode;
    
    @JsonProperty("MailingLatitude")
    private String mailingLatitude;
    
    @JsonProperty("MailingLongitude")
    private String mailingLongitude;
    
    @JsonProperty("MailingGeocodeAccuracy")
    private String mailingGeocodeAccuracy;
    
    @JsonProperty("Phone")
    private String phone;
    
    @JsonProperty("Fax")
    private String fax;
    
    @JsonProperty("MobilePhone")
    private String mobilePhone;
    
}