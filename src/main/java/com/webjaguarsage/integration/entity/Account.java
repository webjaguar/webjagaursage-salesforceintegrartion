package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class Account extends SFObject {
	
	@JsonProperty("Name")
    private String name;
	
	@JsonProperty("Industry")
    private String industry;
	
	@JsonProperty("Type")
    private String type;
	
	@JsonProperty("BillingStreet")
    private String billingStreet;
	
	@JsonProperty("BillingCity")
    private String billingCity;
	
	@JsonProperty("BillingState")
    private String billingState;
	
	@JsonProperty("BillingStateCode")
    private String billingStateCode;
	
	@JsonProperty("BillingPostalCode")
    private String billingPostalCode;
	
	@JsonProperty("BillingCountry")
    private String billingCountry;
	
	@JsonProperty("BillingCountryCode")
    private String billingCountryCode;
	
	@JsonProperty("BillingLatitude")
    private String billingLatitude;
	
	@JsonProperty("BillingLongitude")
    private String billingLongitude;
	
	@JsonProperty("BillingGeocodeAccuracy")
    private String billingGeocodeAccuracy;
	
	@JsonProperty("ShippingStreet")
    private String shippingStreet;
	
	@JsonProperty("ShippingCity")
    private String shippingCity;
	
	@JsonProperty("ShippingState")
    private String shippingState;
	
	@JsonProperty("ShippingPostalCode")
    private String shippingPostalCode;
	
	@JsonProperty("ShippingCountry")
    private String shippingCountry;
	
	@JsonProperty("ShippingLatitude")
    private String shippingLatitude;
	
	@JsonProperty("ShippingLongitude")
    private String shippingLongitude;
	
	@JsonProperty("ShippingGeocodeAccuracy")
    private String shippingGeocodeAccuracy;
	
	@JsonProperty("Phone")
    private String phone;
	
	@JsonProperty("Fax")
    private String fax;
	
	@JsonProperty("Website")
    private String website;
	
	@JsonProperty("PersonContactId")
	private String personContactId;
}