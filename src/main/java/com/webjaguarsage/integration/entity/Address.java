package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class Address {
    private String city;
    private String country;
    private String countryCode;
    private String geocodeAccuracy;
    private String postalCode;
    private String state;
    private String stateCode;
    private String street;

}
