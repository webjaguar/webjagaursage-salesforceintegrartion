package com.webjaguarsage.integration.entity;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SalesOrderList extends SFObject {
	
	private int totalSize;

	@JsonProperty("records")
    private SalesOrder[] salesOrders;
}
