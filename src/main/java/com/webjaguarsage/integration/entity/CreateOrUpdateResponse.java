package com.webjaguarsage.integration.entity;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class CreateOrUpdateResponse {

    private String id;
	
    private boolean success;
    
    private ArrayList<Object> errors;
    
    private String jsonResponse;
}