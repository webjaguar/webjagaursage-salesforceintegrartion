package com.webjaguarsage.integration.entity;

import com.webjaguarsage.integration.SageClient;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class CompositeRequestRecord {
    private String method;
    private String url;
    private String referenceId;
    private Object body;
}
