package com.webjaguarsage.integration.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by soujanya
 */
@Getter
@Setter
public class OpportunityContactRole {
    private String contactId;
    private Boolean isPrimary;
    private String opportunityId;
}
