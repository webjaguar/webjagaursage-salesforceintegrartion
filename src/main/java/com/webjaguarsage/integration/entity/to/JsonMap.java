package com.webjaguarsage.integration.entity.to;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashMap;

/**
 * Created by soujanya
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class JsonMap extends HashMap<String,Object> {}
